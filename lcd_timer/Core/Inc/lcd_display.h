/*
 * lcd_display.h
 *
 *  Created on: Mar 25, 2023
 *      Author: root
 */

#ifndef INC_LCD_DISPLAY_H_
#define INC_LCD_DISPLAY_H_

#include "stm32f4xx_hal.h"
#include <string>

struct _LCD_ADDRESSES
{
	_LCD_ADDRESSES(const uint8_t &device_address) : DEVICE_ADDR(device_address) {}

	/* Command */
	const uint8_t LCD_CLEARDISPLAY = 0x01;
	const uint8_t LCD_RETURNHOME = 0x02;
	const uint8_t LCD_ENTRYMODESET = 0x04;
	const uint8_t LCD_DISPLAYCONTROL = 0x08;
	const uint8_t LCD_CURSORSHIFT = 0x10;
	const uint8_t LCD_FUNCTIONSET = 0x20;
	const uint8_t LCD_SETCGRAMADDR = 0x40;
	const uint8_t LCD_SETDDRAMADDR = 0x80;

	/* Entry Mode */
	const uint8_t LCD_ENTRYRIGHT = 0x00;
	const uint8_t LCD_ENTRYLEFT = 0x02;
	const uint8_t LCD_ENTRYSHIFTINCREMENT = 0x01;
	const uint8_t LCD_ENTRYSHIFTDECREMENT = 0x00;

	/* Display On/Off */
	const uint8_t LCD_DISPLAYON = 0x04;
	const uint8_t LCD_DISPLAYOFF = 0x00;
	const uint8_t LCD_CURSORON = 0x02;
	const uint8_t LCD_CURSOROFF = 0x00;
	const uint8_t LCD_BLINKON = 0x01;
	const uint8_t LCD_BLINKOFF = 0x00;

	/* Cursor Shift */
	const uint8_t LCD_DISPLAYMOVE = 0x08;
	const uint8_t LCD_CURSORMOVE = 0x00;
	const uint8_t LCD_MOVERIGHT = 0x04;
	const uint8_t LCD_MOVELEFT = 0x00;

	/* Function Set */
	const uint8_t LCD_8BITMODE = 0x10;
	const uint8_t LCD_4BITMODE = 0x00;
	const uint8_t LCD_2LINE = 0x08;
	const uint8_t LCD_1LINE = 0x00;
	const uint8_t LCD_5x10DOTS = 0x04;
	const uint8_t LCD_5x8DOTS = 0x00;

	/* Backlight */
	const uint8_t LCD_BACKLIGHT = 0x08;
	const uint8_t LCD_NOBACKLIGHT = 0x00;

	/* Enable Bit */
	const uint8_t ENABLE = 0x04;

	/* Read Write Bit */
	const uint8_t RW = 0x0;

	/* Register Select Bit */
	const uint8_t RS = 0x01;

	/* Device I2C Address */
	const uint8_t DEVICE_ADDR;
};

class I2CDisplayHD44780
{

public:
	struct DisplayDimension
	{
		uint8_t rows;
		uint8_t cols;
	};

	I2CDisplayHD44780(DisplayDimension dimension, const uint8_t &device_address)
		: _display_dimension(dimension),
		  _device_addresses(device_address)
	{
		_Init(dimension.rows);
	}

	void Clear();
	void Home();
	void NoDisplay();
	void Display();
	void NoBlink();
	void Blink();
	void NoCursor();
	void Cursor();
	void ScrollDisplayLeft();
	void ScrollDisplayRight();
	void PrintLeft();
	void PrintRight();
	void LeftToRight();
	void RightToLeft();
	void ShiftIncrement();
	void ShiftDecrement();
	void NoBacklight();
	void Backlight();
	void AutoScroll();
	void NoAutoScroll();
	void CreateSpecialChar(uint8_t, uint8_t[]);
	void PrintSpecialChar(uint8_t);
	void SetCursor(uint8_t, uint8_t);
	void SetBacklight(uint8_t new_val);
	void LoadCustomCharacter(uint8_t char_num, uint8_t *rows);
	void PrintStr(const char[]);
	void WriteMessage(const std::string message);

private:
	void _Init(uint8_t rows);

	void _DelayUS(uint32_t us);
	void _DelayInit(void);
	void _PulseEnable(uint8_t _data);
	void _ExpanderWrite(uint8_t _data);
	void _Write4Bits(uint8_t value);
	void _Send(uint8_t value, uint8_t mode);
	void _SendChar(uint8_t ch);
	void _SendCommand(uint8_t cmd);

	DisplayDimension _display_dimension;
	_LCD_ADDRESSES _device_addresses;
};

#endif /* INC_LCD_DISPLAY_H_ */
