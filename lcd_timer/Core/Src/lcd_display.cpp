/*
 * _device_addresses.LCD_display.cpp
 *
 *  Created on: Mar 25, 2023
 *      Author: root
 */

#include "lcd_display.h"

extern I2C_HandleTypeDef hi2c1;

namespace
{

  uint8_t dpFunction;
  uint8_t dpControl;
  uint8_t dpMode;
  uint8_t dpRows;
  uint8_t dpBacklight;

  uint8_t special1[8] = {
      0b00000,
      0b11001,
      0b11011,
      0b00110,
      0b01100,
      0b11011,
      0b10011,
      0b00000};

  uint8_t special2[8] = {
      0b11000,
      0b11000,
      0b00110,
      0b01001,
      0b01000,
      0b01001,
      0b00110,
      0b00000};

}

void I2CDisplayHD44780::_Init(uint8_t rows)
{
  dpRows = rows;

  dpBacklight = _device_addresses.LCD_BACKLIGHT;

  dpFunction = _device_addresses.LCD_4BITMODE | _device_addresses.LCD_1LINE | _device_addresses.LCD_5x8DOTS;

  if (dpRows > 1)
  {
    dpFunction |= _device_addresses.LCD_2LINE;
  }
  else
  {
    dpFunction |= _device_addresses.LCD_5x10DOTS;
  }

  /* Wait for initialization */
  _DelayInit();
  HAL_Delay(50);

  _ExpanderWrite(dpBacklight);
  HAL_Delay(1000);

  /* 4bit Mode */
  _Write4Bits(0x03 << 4);
  _DelayUS(4500);

  _Write4Bits(0x03 << 4);
  _DelayUS(4500);

  _Write4Bits(0x03 << 4);
  _DelayUS(4500);

  _Write4Bits(0x02 << 4);
  _DelayUS(100);

  /* Display Control */
  _SendCommand(_device_addresses.LCD_FUNCTIONSET | dpFunction);

  dpControl = _device_addresses.LCD_DISPLAYON | _device_addresses.LCD_CURSOROFF | _device_addresses.LCD_BLINKOFF;
  Display();
  Clear();

  /* Display Mode */
  dpMode = _device_addresses.LCD_ENTRYLEFT | _device_addresses.LCD_ENTRYSHIFTDECREMENT;
  _SendCommand(_device_addresses.LCD_ENTRYMODESET | dpMode);
  _DelayUS(4500);

  CreateSpecialChar(0, special1);
  CreateSpecialChar(1, special2);

  Home();
}

void I2CDisplayHD44780::Clear()
{
  _SendCommand(_device_addresses.LCD_CLEARDISPLAY);
  _DelayUS(2000);
}

void I2CDisplayHD44780::Home()
{
  _SendCommand(_device_addresses.LCD_RETURNHOME);
  _DelayUS(2000);
}

void I2CDisplayHD44780::SetCursor(uint8_t col, uint8_t row)
{
  int row_offsets[] = {0x00, 0x40, 0x14, 0x54};
  if (row >= dpRows)
  {
    row = dpRows - 1;
  }
  _SendCommand(_device_addresses.LCD_SETDDRAMADDR | (col + row_offsets[row]));
}

void I2CDisplayHD44780::NoDisplay()
{
  dpControl &= ~_device_addresses.LCD_DISPLAYON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::Display()
{
  dpControl |= _device_addresses.LCD_DISPLAYON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::NoCursor()
{
  dpControl &= ~_device_addresses.LCD_CURSORON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::Cursor()
{
  dpControl |= _device_addresses.LCD_CURSORON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::NoBlink()
{
  dpControl &= ~_device_addresses.LCD_BLINKON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::Blink()
{
  dpControl |= _device_addresses.LCD_BLINKON;
  _SendCommand(_device_addresses.LCD_DISPLAYCONTROL | dpControl);
}

void I2CDisplayHD44780::ScrollDisplayLeft(void)
{
  _SendCommand(_device_addresses.LCD_CURSORSHIFT | _device_addresses.LCD_DISPLAYMOVE | _device_addresses.LCD_MOVELEFT);
}

void I2CDisplayHD44780::ScrollDisplayRight(void)
{
  _SendCommand(_device_addresses.LCD_CURSORSHIFT | _device_addresses.LCD_DISPLAYMOVE | _device_addresses.LCD_MOVERIGHT);
}

void I2CDisplayHD44780::LeftToRight(void)
{
  dpMode |= _device_addresses.LCD_ENTRYLEFT;
  _SendCommand(_device_addresses.LCD_ENTRYMODESET | dpMode);
}

void I2CDisplayHD44780::RightToLeft(void)
{
  dpMode &= ~_device_addresses.LCD_ENTRYLEFT;
  _SendCommand(_device_addresses.LCD_ENTRYMODESET | dpMode);
}

void I2CDisplayHD44780::AutoScroll(void)
{
  dpMode |= _device_addresses.LCD_ENTRYSHIFTINCREMENT;
  _SendCommand(_device_addresses.LCD_ENTRYMODESET | dpMode);
}

void I2CDisplayHD44780::NoAutoScroll(void)
{
  dpMode &= ~_device_addresses.LCD_ENTRYSHIFTINCREMENT;
  _SendCommand(_device_addresses.LCD_ENTRYMODESET | dpMode);
}

void I2CDisplayHD44780::CreateSpecialChar(uint8_t location, uint8_t charmap[])
{
  location &= 0x7;
  _SendCommand(_device_addresses.LCD_SETCGRAMADDR | (location << 3));
  for (int i = 0; i < 8; i++)
  {
    _SendChar(charmap[i]);
  }
}

void I2CDisplayHD44780::PrintSpecialChar(uint8_t index)
{
  _SendChar(index);
}

void I2CDisplayHD44780::LoadCustomCharacter(uint8_t char_num, uint8_t *rows)
{
  CreateSpecialChar(char_num, rows);
}

void I2CDisplayHD44780::PrintStr(const char c[])
{
  while (*c)
    _SendChar(*c++);
}

void I2CDisplayHD44780::WriteMessage(const std::string message)
{
  Clear();
  Home();
  if (!message.empty())
  {
    PrintStr(message.substr(0, _display_dimension.cols * _display_dimension.rows).c_str());
  }
}

void I2CDisplayHD44780::SetBacklight(uint8_t new_val)
{
  if (new_val)
    Backlight();
  else
    NoBacklight();
}

void I2CDisplayHD44780::NoBacklight(void)
{
  dpBacklight = _device_addresses.LCD_NOBACKLIGHT;
  _ExpanderWrite(0);
}

void I2CDisplayHD44780::Backlight(void)
{
  dpBacklight = _device_addresses.LCD_BACKLIGHT;
  _ExpanderWrite(0);
}

void I2CDisplayHD44780::_SendCommand(uint8_t cmd)
{
  _Send(cmd, 0);
}

void I2CDisplayHD44780::_SendChar(uint8_t ch)
{
  _Send(ch, _device_addresses.RS);
}

void I2CDisplayHD44780::_Send(uint8_t value, uint8_t mode)
{
  uint8_t highnib = value & 0xF0;
  uint8_t lownib = (value << 4) & 0xF0;
  _Write4Bits((highnib) | mode);
  _Write4Bits((lownib) | mode);
}

void I2CDisplayHD44780::_Write4Bits(uint8_t value)
{
  _ExpanderWrite(value);
  _PulseEnable(value);
}

void I2CDisplayHD44780::_ExpanderWrite(uint8_t _data)
{
  uint8_t data = _data | dpBacklight;
	HAL_I2C_Master_Transmit(&hi2c1, _device_addresses.DEVICE_ADDR, (uint8_t *)&data, 1, 10);
}

void I2CDisplayHD44780::_PulseEnable(uint8_t _data)
{
  _ExpanderWrite(_data | _device_addresses.ENABLE);
  _DelayUS(20);

  _ExpanderWrite(_data & ~_device_addresses.ENABLE);
  _DelayUS(20);
}

void I2CDisplayHD44780::_DelayInit(void)
{
  CoreDebug->DEMCR &= ~CoreDebug_DEMCR_TRCENA_Msk;
  CoreDebug->DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;

  DWT->CTRL &= ~DWT_CTRL_CYCCNTENA_Msk; //~0x00000001;
  DWT->CTRL |= DWT_CTRL_CYCCNTENA_Msk;  // 0x00000001;

  DWT->CYCCNT = 0;

  /* 3 NO OPERATION instructions */
  __ASM volatile("NOP");
  __ASM volatile("NOP");
  __ASM volatile("NOP");
}

void I2CDisplayHD44780::_DelayUS(uint32_t us)
{
  uint32_t cycles = (SystemCoreClock / 1000000L) * us;
  uint32_t start = DWT->CYCCNT;
  volatile uint32_t cnt;

  do
  {
    cnt = DWT->CYCCNT - start;
  } while (cnt < cycles);
}
